class Chopper
  def chop(position, array)
    return array.index(position) if array.include?(position)

    -1
  end

  def sum(array)
    return 'vacio' if array.empty?

    sum = 0
    array.each { |a| sum += a }

    return 'demasiado grande' if sum >= 100

    arg1 = sum.div(10)
    arg2 = sum.modulo(10)

    return num2name(arg1) + ',' + num2name(arg2) unless arg1.zero?

    num2name(arg2)
  end

  private

  def num2name(number)
    numbers_name = {
      1 => 'uno',
      2 => 'dos',
      3 => 'tres',
      4 => 'cuatro',
      5 => 'cinco',
      6 => 'seis',
      7 => 'siete',
      8 => 'ocho',
      9 => 'nueve',
      0 => 'cero'
    }

    numbers_name[number]
  end
end
