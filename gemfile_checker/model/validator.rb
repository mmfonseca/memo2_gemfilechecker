
require_relative './exceptions'

class Validator

  def process(gemfile_content)
    
    !checkEmpty(gemfile_content) && 
    checkSource(gemfile_content) && 
    checkRuby(gemfile_content) &&
    checkOrderGems(gemfile_content)     
    
  end

  def checkEmpty(gemfile_content)
    if gemfile_content.empty? 
      raise FormatGemfileError.new("Error: Gemfile vacio")
    end
    false
  end

  def checkSource(gemfile_content)
    if !gemfile_content.include? "source" 
      raise FormatGemfileError.new("Error: Gemfile sin source")
    end
    true
  end

  def checkRuby(gemfile_content)
    if !gemfile_content.include? "ruby "
      raise FormatGemfileError.new("Gemfile sin version de ruby")
    end
    true
  end

  def checkOrderGems(gemfile_content)
    gem = ""
    first_gem = true
    
    gemfile_content.each_line do |line|
      if line.include? "gem "
        if first_gem
          first_gem = false
          gem = line.split(' ')[1]
          next
        end
        gemCurrent = line.split(' ')[1]  
        if gemCurrent < gem
          raise FormatGemfileError.new("Gemfile con gemas desordenadas")
          break
        end
        gem = gemCurrent
      end
    end
    true  
  end

end
