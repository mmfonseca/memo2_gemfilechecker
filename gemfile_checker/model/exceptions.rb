class FormatGemfileError < StandardError
  
    def initialize(message, action)
      super(message)
    end
  end
  
