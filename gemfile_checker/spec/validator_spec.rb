require 'rspec'
require_relative '../model/validator'

describe Validator do

  subject { @validator = Validator.new }

  it { should respond_to (:process) }

  it 'should validate gemfile string and return boolean' do
    gemfile_string = ''
    begin
      result = subject.process gemfile_string
    rescue => e
      e.message == 'Error: Gemfile vacio'
    end
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.valid"
    validation_result = Validator.new.process gemfile_content
    expect(validation_result).to eq true
  end

  
  it 'should return Not found source when Gemfile does not have Source' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.source"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue => e
      e.message == 'Error: Gemfile sin source'
    end

  end

  it 'should return Not found ruby version when Gemfile does not have Ruby version' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.ruby"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue => e
      e.message == 'Gemfile sin version de ruby'
    end
  end

  it 'should return Gems incorrect order when Gems are in incorrect order' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.gems"
    begin
      validation_result = Validator.new.process gemfile_content
    rescue => e
      e.message == 'Gemfile con gemas desordenadas'
    end
  end

end
