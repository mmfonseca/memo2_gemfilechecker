# parser => file_reader
# validator => model
# presenter => print
require_relative './model/validator'

gemfile_content = File.read ARGV[0]

begin
  Validator.new.process gemfile_content
  puts 'Gemfile correcto'
rescue => e
  puts e
end

